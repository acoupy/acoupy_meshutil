import typing
import pathlib
import logging
import meshio
import traceback
import tempfile
import fenics
import enum
import numpy as np

# python3 -m venv --system-site-packages venv
# pip install --no-binary=h5py h5py
# https://jsdokken.com/src/pygmsh_tutorial.html
# https://fenicsproject.discourse.group/t/working-with-imported-meshes/9079

logger = logging.getLogger(__name__)


class Reader:

    class Format(enum.Enum):
        """
        This enumerator is used to select the file format. Only the formats listed here are supported.
        """
        MED = 0
        """Selects MED File (Salome Platform)."""

    class Dimension(enum.Enum):
        """
        This enumerator is used to select the geometrical dimension of the mesh.
        """
        D_1D = 1
        """Selects ``1D`` Mesh."""
        D_2D = 2
        """Selects ``2D`` Mesh."""
        D_3D = 3
        """Selects ``3D`` Mesh."""

    def __init__(self, file_format: Format, dimension: Dimension, coordinate_scaling: float = 1.0) -> None:
        """
        Class to read mesh files.

        :param file_format: The format of the files to be read by this reader.
        :type file_format: :class:`.Format`
        :param dimension: The geometrical dimension of the files to be read by this reader.
        :type dimension: :class:`.Dimension`
        :param coordinate_scaling: The resulting mesh coordinate values will be interpreted as having :math:`\\text{m}`
                                   units. If the mesh file has other units, specify a scaling factor with this argument.
                                   By default, this argument is ``1``.
        :type coordinate_scaling: :class:`float`

        :Example:

        >>> # Let's make a reader for 2D MED files with mm coordinates.
        >>> from acoupy_meshutil import readers
        >>> reader = readers.Reader(readers.Reader.Format.MED, readers.Reader.Dimension.D_2D, 1e-3)
        """
        logger.info('Crated Reader Object at %d', id(self))

        self._file_format: Reader.Format = file_format
        self._dimension: Reader.Dimension = dimension
        self._coordinate_scaling: float = coordinate_scaling

        if self._file_format == Reader.Format.MED:
            self._cell_data_id: str = 'cell_tags'
        else:
            logger.fatal('Requested File Format Support is Not Implemented')
            raise NotImplemented('Requested File Format Support is Not Implemented')

        self._cell_tags: dict = {}

        self._mesh: meshio.Mesh = fenics.Mesh()

        # The entities must be specified in the `_entities` list in decreasing order of dimensionality,
        # as the higher dimension one must be read first
        if self._dimension == Reader.Dimension.D_1D:
            self._entities: typing.List[str] = ['line']
            self._expect_pruning: bool = True
            self._prune_count: int = 2
            self._mvc_d: fenics.MeshValueCollection = fenics.MeshValueCollection('size_t', self._mesh, 1)
            self._mvc_sd: typing.List[fenics.MeshValueCollection] = []
        if self._dimension == Reader.Dimension.D_2D:
            self._entities: typing.List[str] = ['triangle', 'line']
            self._expect_pruning: bool = True
            self._prune_count: int = 1
            self._mvc_d: fenics.MeshValueCollection = fenics.MeshValueCollection('size_t', self._mesh, 2)
            self._mvc_sd: typing.List[fenics.MeshValueCollection] = [
                fenics.MeshValueCollection('size_t', self._mesh, 1)
            ]
        if self._dimension == Reader.Dimension.D_3D:
            self._entities: typing.List[str] = ['tetra', 'triangle', 'line']
            self._expect_pruning: bool = False
            self._prune_count: int = 0
            self._mvc_d: fenics.MeshValueCollection = fenics.MeshValueCollection('size_t', self._mesh, 3)
            self._mvc_sd: typing.List[fenics.MeshValueCollection] = [
                fenics.MeshValueCollection('size_t', self._mesh, 2),
                fenics.MeshValueCollection('size_t', self._mesh, 1)
            ]

        self._name_to_read: str = 'marker'

        self._cell_tag_offset: int = 0

    def _create_mesh(
            self,
            mesh: meshio.Mesh,
            cell_type_id: str,
            cell_data_id: str,
            prune_mask: np.ndarray,
    ) -> meshio.Mesh:
        assert prune_mask.dtype == bool, '`prune_mask` must be `bool` array'
        assert prune_mask.size == 3, '`prune_mask` must be 1D array of size 3'
        assert prune_mask.shape == (3, ), '`prune_mask` must be 1D array of size 3'
        assert np.sum(np.logical_not(prune_mask)) == self._prune_count, 'Incompatible number of pruned dimensions'

        cells = mesh.get_cells_type(cell_type_id)
        cell_data = mesh.get_cell_data(cell_data_id, cell_type_id)
        points = mesh.points[:, prune_mask] * self._coordinate_scaling

        out_mesh = meshio.Mesh(
            points=points,
            cells={cell_type_id: cells},
            cell_data={self._name_to_read: [cell_data + self._cell_tag_offset]}
        )

        return out_mesh

    def read(self, path: pathlib.Path, prune_mask: typing.Optional[np.ndarray] = None) -> typing.Tuple[bool, Exception]:
        """
        Read a mesh file.

        :param path: path to the mesh file to be read.
        :type path: :class:`pathlib.Path`
        :param prune_mask: Optional. For ``1D`` and ``2D`` meshes, use this argument to specify which dimensions to
                           prune.
                           This argument is an array of 3 booleans, each for :math:`x`, :math:`y` and :math:`z`.
                           The items set to ``False`` are pruned. The various valid pruning and defaults for each mesh
                           dimension are reported below.

                           - ``1D``: 2 dimensions have to be pruned. By default, `numpy.array([True, False, False])``.
                           - ``2D``: 1 dimension has to be pruned. By default, `numpy.array([True, True, False])``.
                           - ``3D``: no pruning.
        :type prune_mask: :class:`numpy.ndarray` with :class:`bool` ``dtype``
        :return: A tuple with a boolean and eventual exception. The boolean is ``True`` if the operation was successful.
                 In that case the exception is blank. Otherwise, the boolean is ``False`` and the exception contains the
                 encountered error.
        :rtype: :class:`Tuple[bool, Exception]`

        :Example:

        >>> # Let's read a 2D mesh file called `mesh.med`, in home. Let's assume the mesh in saved in mm and that it
        >>> # is defined on the y-z plane (x needs pruning).
        >>> from acoupy_meshutil import readers
        >>> import pathlib
        >>> import numpy
        >>> reader = readers.Reader(readers.Reader.Format.MED, readers.Reader.Dimension.D_2D, 1e-3)
        >>> mesh_path = pathlib.Path().home().joinpath('mesh.med')
        >>> success, exception = reader.read(mesh_path, prune_mask=numpy.array([False, True, True]))
        >>> # Your code can do clever things if things go wrong. Here we just raise the exception.
        >>> if not success:
        ...     raise exception
        """

        if prune_mask is None:
            if self._dimension == Reader.Dimension.D_1D:
                prune_mask = np.array([True, False, False])
            if self._dimension == Reader.Dimension.D_2D:
                prune_mask = np.array([True, True, False])
            if self._dimension == Reader.Dimension.D_3D:
                prune_mask = np.array([True, True, True])

        try:

            raw_mesh_data = meshio.read(path)
            raw_cell_tags = raw_mesh_data.cell_tags
            # If the ID are negative make them all positive by adding an appropriate offset which is computed here.
            min_cell_tag_id = np.min(list(raw_cell_tags.keys()))
            if min_cell_tag_id < 0:
                self._cell_tag_offset = np.abs(min_cell_tag_id)
            else:
                self._cell_tag_offset = 0
            self._cell_tags = {k + self._cell_tag_offset: raw_cell_tags[k] for k in raw_cell_tags}

            with tempfile.TemporaryDirectory() as xdmf_dir:
                target_xdmf_dir = pathlib.Path(xdmf_dir)

                for e in self._entities:
                    entity_mesh_data = self._create_mesh(
                        mesh=raw_mesh_data,
                        cell_type_id=e,
                        cell_data_id=self._cell_data_id,
                        prune_mask=prune_mask
                    )

                    tmp_path = target_xdmf_dir.joinpath(e + '_mesh.xdmf')
                    meshio.write(tmp_path.absolute().as_posix(), entity_mesh_data)
                    with fenics.XDMFFile(tmp_path.absolute().as_posix()) as xdmf_file:

                        if self._dimension == Reader.Dimension.D_3D:
                            if e == 'tetra':
                                xdmf_file.read(self._mesh)
                                xdmf_file.read(self._mvc_d, self._name_to_read)
                            if e == 'triangle':
                                xdmf_file.read(self._mvc_sd[0], self._name_to_read)
                            if e == 'line':
                                xdmf_file.read(self._mvc_sd[1], self._name_to_read)

                        if self._dimension == Reader.Dimension.D_2D:
                            if e == 'triangle':
                                xdmf_file.read(self._mesh)
                                xdmf_file.read(self._mvc_d, self._name_to_read)
                            if e == 'line':
                                xdmf_file.read(self._mvc_sd[0], self._name_to_read)

                        if self._dimension == Reader.Dimension.D_1D:
                            if e == 'line':
                                xdmf_file.read(self._mesh)
                                xdmf_file.read(self._mvc_d, self._name_to_read)

            return True, Exception()

        except Exception as e:
            logger.exception(traceback.format_exc())
            return False, e

    def get_mesh(self) -> typing.Tuple[fenics.Mesh, fenics.MeshFunction, typing.List[fenics.MeshFunction]]:
        """
        Get the mesh that was read. This mesh id returned alongside body markers and subdomains for the lower dimension
        objects.

        :return: A ``Tuple`` if the following:

                 - A :class:`fenics.Mesh` object containing the mesh proper.
                 - A :class:`fenics.MeshFunction` object returning the different bodies IDs over the mesh.
                 - A ``List`` of :class:`fenics.MeshFunction` objects, each for a lower dimension subdomain.
                   They return the IDs of different parts of the subdomains. The list has:

                   - 2 elements for ``3D`` meshes, element ``0`` is for ``2D`` subdomains, element ``1`` is for ``1D``
                     subdomains.
                   - 1 element for ``2D`` meshes, element ``0`` is for ``1D`` subdomains.
                   - No elements for ``1D`` meshes.

        :rtype: :class:`Tuple[fenics.Mesh, fenics.MeshFunction, List[fenics.MeshFunction]]`

        :Example:

        >>> # Let's read a 3D mesh file called `mesh.med`, in home. Let's assume the mesh in saved in m
        >>> from acoupy_meshutil import readers
        >>> import pathlib
        >>> import numpy
        >>> import fenics
        >>> reader = readers.Reader(readers.Reader.Format.MED, readers.Reader.Dimension.D_3D)
        >>> mesh_path = pathlib.Path().home().joinpath('mesh.med')
        >>> success, exception = reader.read(mesh_path, prune_mask=numpy.array([False, True, True]))
        >>> # Your code can do clever things if things go wrong. Here we just raise the exception.
        >>> if not success:
        ...     raise exception
        >>> # Now, let's get the data.
        >>> mesh, body_markers, sub_domains_markers = reader.get_mesh()
        >>> # We can use the objects to define measures as follows:
        >>> dx_volumes = fenics.Measure('dx', domain=mesh, subdomain_data=body_markers)
        >>> ds_surfaces = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains_markers[0])
        >>> ds_lines = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains_markers[1])
        >>> # Each of the object above will take an integer ID and return the measure over the subdomain at that ID.
        >>> # For example, for a ds over surface ID 2:
        >>> ds_surface_2 = ds_surfaces(2)
        >>> # To see the various IDs, use:
        >>> ids = reader.get_cell_tags()
        >>> # `ids` is a dictionary mapping IDs to their name.
        """
        markers = fenics.MeshFunction('size_t', self._mesh, self._mvc_d)
        sub_domains = [fenics.MeshFunction('size_t', self._mesh, sd) for sd in self._mvc_sd]
        return self._mesh, markers, sub_domains

    def get_cell_tags(self) -> dict:
        """
        Return the tags of the mesh cells. Useful to identify the IDs of varius subdomains and boundaries.

        :return: The tags, as a dictionary.
        :rtype: :class:`dict`

        :Example:

        See :func:`get_mesh`.
        """
        return self._cell_tags
