import pathlib
import typing
from acoupy_meshutil import readers


def get_sphere_cut_mesh() -> typing.Tuple[readers.Reader, typing.Dict[str, float]]:
    """
    Get a preloaded reader for an example mesh for a pulsating sphere problem.
    The mesh consists of a large sphere (:math:`0.343 \\text{ m}` radius) with a smaller sphere cutout in the center
    (:math:`5 \\text{ mm}` radius). The mesh size (maximum :math:`28 \\text{ mm}`) is suitable to up to
    :math:`1 \\text{ kHz}` for air at :math:`20 \\text{ } ^{\\circ}\\text{C}` . A dictionary containing all the mesh
    details is also provided.

    :return: A tuple containing a :class:`.Reader` object and a :class:`Dict` metadata object.
    :rtype: :class:`Tuple[readers.Reader, Dict[str, float]]`

    :Example:

    >>> from acoupy_meshutil import builtin
    >>> import fenics
    >>> reader_obj, metadata = builtin.get_sphere_cut_mesh()
    >>> print(metadata)
    {'inner_radius': 0.005, 'outer_radius': 0.343, 'mesh_max_size': 0.028, 'mesh_min_size': 0.001}
    >>> mesh, body_markers, sub_domains_markers = reader_obj.get_mesh()
    >>> tags = reader_obj.get_cell_tags()
    >>> print(tags)
    {1: ['inner'], 0: ['outer']}
    >>> # Define measure over inner surface:
    >>> ds_surfaces = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains_markers[0])
    >>> ds_inner = ds_surfaces(1)
    >>> # And over outer:
    >>> ds_outer = ds_surfaces(0)
    """

    reader = readers.Reader(
        file_format=readers.Reader.Format.MED,
        dimension=readers.Reader.Dimension.D_3D
    )

    reader.read(
        pathlib.Path(__file__).parent.joinpath('data', 'meshes', 'med', 'mid_spherecut_m.med')
    )

    meta = {
        'inner_radius': 0.005,
        'outer_radius': 0.343,
        'mesh_max_size': 0.028,
        'mesh_min_size': 0.001
    }

    return reader, meta
