import setuptools

# Read the README.md file to add it to the long_description attribute below
with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

setuptools.setup(
    name='acoupy_meshutil',
    version='0.0.3',
    author='Stefano Tronci',
    author_email='stefano.tronci@protonmail.com',
    description='Utilities for meshes.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/acoupy/acoupy_meshutil',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
    install_requires=[
        'numpy',
        'meshio',
        'h5py'
    ],
    extras_require={
        'doc': [
            'Sphinx!=5.2.0.post0',
            'karma-sphinx-theme'
        ]
    },
    package_data={
        'acoupy_meshutil': [
            'data/meshes/med/mid_spherecut_m.med'
        ]
    },
)
