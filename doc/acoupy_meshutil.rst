acoupy\_meshutil package
========================

Submodules
----------

acoupy\_meshutil.readers module
-------------------------------

.. automodule:: acoupy_meshutil.readers
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: acoupy_meshutil
   :members:
   :undoc-members:
   :show-inheritance:
