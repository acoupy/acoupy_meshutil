.. _tut-salomemed:

===================
Salome MED
===================

In this tutorial we will show how to create a suitable ``MED`` mesh file by using ``Salome 9.9.0``. For more
information about ``Salome`` visit https://www.salome-platform.org/.

----------------------
Goal
----------------------

In this tutorial we will create a simple domain consisting of two concentric spheres. This can be a domain for
acoustic simulations in which the innermost domain is where our simulation is. The outer shell instead might house
an adiabatic absorber. This way the inner volume is anechoically terminated. This multi-solids approach allows us to
to show how to use all of the mesh functions returned by :func:`acoupy_meshutil.readers.Reader.get_mesh`.

----------------------
Geometry
----------------------

We will define our geometry in units of :math:`\text{mm}`. This means we will need to make use of
``coordinate_scaling`` in :class:`acoupy_meshutil.readers.Reader`.

Open ``Salome``. Select ``File`` then ``New``. In the ``Modules`` selector select ``Geometry`` as shown below.

.. image:: /tutorials/res/salomemed/geometry.png

Select ``New Entity``, ``Primitives`` and ``Sphere``. Alternatively, simply click the ``Create a sphere`` button
as shown below. Both these actions lead to the ``Sphere Construction`` window.

.. image:: /tutorials/res/salomemed/createasphere.png

In the ``Sphere Construction`` window, use ``inner`` as a ``Name`` and ``400`` as a ``Radius``. You can call your
entity as you like, but it is best to use reasonable names as we will identify bodies by name later on. The inner
sphere construction settings are shown below. When done, click ``Apply and Close``.

.. image:: /tutorials/res/salomemed/sphereconstruction.png

Follow the same procedure to create a new sphere of ``Radius`` ``500`` and ``Name`` ``outer``. Your ``Object Browser``
will look like this:

.. image:: /tutorials/res/salomemed/objectbrowser.png

Select the ``outer`` entity from the ``Object Browser``. Then, choose ``Operations`` and ``Partition``. Alternatively,
click the ``Partition`` button shown below. Both these actions lead to the ``Partition of Object with Tool`` window.

.. image:: /tutorials/res/salomemed/partition.png

In the ``Partition of Object with Tool`` window, put ``domain`` in the ``Name`` field. Then, click the arrow button
next to ``Tool Objects`` and select ``inner`` from the ``Object Browser``. Choose ``Solid`` as a ``Resulting Type``.
The settings are shown below. When done, click ``Apply and Close``.

.. image:: /tutorials/res/salomemed/partitionofobjectwithtool.png

This will result in a new ``domain`` entity as shown below in the ``Object Browser``.

.. image:: /tutorials/res/salomemed/objectbrowser2.png

Select the ``domain`` entity in the ``Object Browser``. Then, choose ``New Entity`` and ``Explode``. Alternatively,
click the ``Explode`` button as shown below. Both these actions lead to the ``Sub-shapes Selection`` window.

.. image:: /tutorials/res/salomemed/explode.png

In the ``Sub-shapes Selection`` window, select ``Solid`` in the ``Sub-shapes Type`` field. The settings are shown below.
When done, click ``Apply and Close``.

.. image:: /tutorials/res/salomemed/subshapesselection.png

This will crate two new entities, ``Solid_1`` and ``Solid_2``. By clicking on the eye button near the entity name in
the ``Object Browser`` you will be able to verify that ``Solid_1`` is the outer sphere shell, while ``Solid_2`` is the
inner sphere solid. It is best to rename these entities. Right click on the entity to rename in the ``Object Browser``
and choose ``Rename``. In this tutorial we will rename ``Solid_1`` to ``outer`` and ``Solid_2`` to ``inner``.

Select the ``domain`` entity in the ``Object Browser`` and repeat the explode procedure, but this time select
``Face`` in the ``Sub-shapes Selection`` window's ``Sub-shapes Type`` field. This will result in two new entities,
``Face_1`` and ``Face_2``. ``Face_1`` is the outer surface of the inner sphere. ``Face_2`` is the outer surface of the
outer sphere shell. It is best to rename these going forward. In this tutorial these faces are renamed to
``inner_face`` and ``outer_face`` respectively.

By doing so we create a single domain partitioned in 2 solid and 2 faces. Note that the ``inner_face`` is the separation
face between the inner sphere and the outer shell. When we will mesh the domain we will create a mesh that fades with
continuity from the inner sphere to the outer shell (conformal mesh). The nodes on the ``inner_face`` will define the
boundary between the two solids. All the nodes will be marked as belonging to the correct entity, so that we can
make use of them within ``FEniCS``. The resulting hierarchy of entities beneath ``domain`` is shown below.

.. image:: /tutorials/res/salomemed/hierarchy.png

----------------------
Meshing
----------------------

In the ``Modules`` selector select ``Mesh`` as shown below.

.. image:: /tutorials/res/salomemed/meshselect.png

In the ``Object Browser`` select the ``domain`` entity. Then, choose ``Mesh`` and ``Create Mesh``. This will open the
``Create mesh`` window. You can name the mesh as you like. In this tutorial we will type ``domain`` in the ``Name``
field. In the ``Algorithm`` field select ``NETGEN 1D-2D-3D``. The settings are shown below.

.. image:: /tutorials/res/salomemed/basicmeshsets.png

Click on the gear button on the left of ``Hypothesis`` and choose ``NETGEN 3D Parameters``. This will open the
``Hypothesis Construction``. We do not plan of making this mesh functional, so we will just leave the defaults to
allow quick computation. You can select any value for the settings, including the ``Name``. The settings of the
``Hypothesis Construction`` window are shown below.

.. image:: /tutorials/res/salomemed/hypcon.png

Click ``OK`` to close the ``Hypothesis Construction`` and then ``Apply and Close`` in the ``Create mesh`` window.
This will create the ``domain`` mesh with all the mesh groups from geometry already defined. The groups as they appear
in the ``Object Browser`` are shown below.

.. image:: /tutorials/res/salomemed/groups.png

Right click the ``domain`` mesh (evidenced above) and choose ``Compute``. Dismiss the ``Mesh computation succeed``
window. Note that by right clicking on empty space anywhere in the 3D viewer you will be able to access various options
including ``Orientation of Faces`` and ``Clipping``. These options can help visualising if your mesh meets your
expectations.

To finish, right click the ``domain`` mesh and choose ``Export`` and then ``MED file``. Save the mesh to a location
of your choice.

----------------------
Working with The File
----------------------

The mesh file can be imported into ``Python`` as follows:

.. code:: python

   from acoupy_meshutil import readers
   import pathlib
   import fenics

   rdr = readers.Reader(
       file_format=readers.Reader.Format.MED,  # Our mesh is MED, so we use this
       dimension=readers.Reader.Dimension.D_3D,  # Our mesh is 3D, so we use this
       coordinate_scaling=1e-3  # We must use this as the coordinates in the MED file are mm
   )

   # To read the MED file we pass a `pathlib.Path` object to the `read` method (edit to your path):
   success, exception = rdr.read(pathlib.Path('/path/to/mesh/file.med'))
   # This method will return `True` and a blank exception if all went good. `False` and an exception if things went
   # wrong. This allows the caller to handle errors as appropriate. Here we just raise.
   if not success:
       raise exception

   # Now that the file is read we can get the mesh data:
   cell_tags = rdr.get_cell_tags()
   mesh, markers, sub_domains = rdr.get_mesh()

   # `cell_tags` contains a dictionary relating the entity names we chosen in Salome to numerical IDs:
   # {3: ['outer'], 2: ['inner'], 1: ['inner_face'], 0: ['outer_face']}

   # Mesh can be used straight away to construct `FEniCS` vector spaces, such as:
   space = fenics.FunctionSpace(mesh, fenics.FiniteElement(family='P', cell=mesh.ufl_cell(), degree=2))

   # `markers` is a `MeshFunctionSizet` that can be used to select the highest dimension entities (in this case 3D).
   # `sub_domains` is a list of `MeshFunctionSizet` of lower dimension entities (in this case 2D and 1D) that can be
   # each used to select the required sub-entity. Selection happen by numerical ID. This can be used to define measures.

   # For example:

   # Let's define a `FEniCS` measure over the inner sphere:
   dx_volumes = fenics.Measure('dx', domain=mesh, subdomain_data=markers)
   dx_inner = dx_volumes(2)  # We use the ID for the `inner` entity as in `cell_tags`.
   # Let's verify things are correct by integrating 1 over the measure: this should be approximately equal to the inner
   # sphere volume:
   volume_inner = fenics.assemble(1 * dx_inner)
   # This comes out as 0.25427687938552257, very close to 4 * np.pi * (400e-3)**3 / 3 = 0.26808257310632905
   # Inaccuracies are mainly due to mesh size

   # Similarly, let's define a `FEniCS` measure over the outer sphere face:
   ds_surfaces = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains[0])  # Use `0` to select the 2D functions
   ds_outer = ds_surfaces(0)  # We use the ID for the `outer_face` entity as in `cell_tags`.
   # Let's check again with the integral. Integrating 1 over the measure gives the surface:
   surface_outer = fenics.assemble(1 * ds_outer)
   # 3.079177900338639 VS 4 * np.pi * (500e-3)**2 = np.pi - Close enough given the coarse mesh.

The snippet above should allow you to see how to read the mesh and get all the data needed to run ``FEniCS`` simulations.
