.. acoupy_meshutil documentation master file, created by
   sphinx-quickstart on Sun Sep 25 13:24:57 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to acoupy_meshutil's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

``acoupy_meshutil`` is a small utility to read mesh files in ``FEniCS`` format. For a list of supported file formats
see :class:`.Format`.

Note that this is **not** an official ``FEniCS`` project. This code exists mainly for the convenience of the author.

===================
Installation
===================

----------------------
Dependencies
----------------------

Most dependencies are handled automatically. ``FEniCS`` instead must be installed beforehand. For more information see
the `FEniCS documentation <https://fenics.readthedocs.io/en/latest/installation.html>`_.

On some Linux systems you might want to install other dependencies beforehand as well to ensure they are all linked to
the same libraries on the system. See below for specific instructions.

~~~~~~~~~~~~~~~~~~~~~~
On Ubuntu
~~~~~~~~~~~~~~~~~~~~~~

Install the following packages:

.. code:: bash

   sudo apt install python3-h5py python3-meshio

----------------------
Environment
----------------------

If you wish to use a virtual environment you must create it so that the system packages installed above
(including ``FEniCS``) are available. Use:

.. code:: bash

   python3 -m venv --system-site-packages venv


And activate your environment.

----------------------
Installation
----------------------

Use ``pip`` to install the software on your target environment:

.. code:: bash

 pip install git+https://gitlab.com/acoupy/acoupy_meshutil.git

===================
Quickstart
===================

The easiest way to start is by using any of the bulit-in mesh files. See :py:mod:`acoupy_meshutil.builtin`. An example is reported below.

.. code:: python

   from acoupy_meshutil import builtin
   import fenics

   # Obtain a reader for the builtin sphere cut mesh. Refer to `metadata` for info about the mesh.
   reader_obj, metadata = builtin.get_sphere_cut_mesh()
   # Get the mesh, a mesh function for the body markers and mesh functions for the boundary markers.
   mesh, body_markers, sub_domains_markers = reader_obj.get_mesh()
   # Create measures for the various boundaries (just 2 for this mesh).
   ds_surfaces = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains_markers[0])
   ds_inner = ds_surfaces(1)
   ds_outer = ds_surfaces(0)

For more detailed tutorials see :ref:`tutorials-index`.
