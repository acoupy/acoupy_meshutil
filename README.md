# acoupy_meshutil

Utilities to import meshes from file into `FEniCS` format. Part of the [`acoupy`](https://gitlab.com/acoupy) project.

## Disclaimer

This project is developed for the author's convenience and purposes. Best efforts will be made to properly maintain it, but do not expect prompt fixes, updates and support. This software is distributed with the [`MIT License`](LICENSE). Quoting from the license:

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Installation

### Dependencies

Most dependencies are handled automatically. `FEniCS` instead must be installed beforehand. For more information see the [`FEniCS` documentation](https://fenics.readthedocs.io/en/latest/installation.html). 

On some Linux systems you might want to install other dependencies beforehand as well to ensure they are all linked to the same libraries on the system. See below for specific instructions.

#### On Ubuntu

Install the following packages:

```commandline
sudo apt install python3-h5py python3-meshio
```

### Environment

If you wish to use a virtual environment you must create it so that the system packages installed above (including `FEniCS`) are available. Use:

```commandline
python3 -m venv --system-site-packages venv
```

And activate your environment.

### Installation

Use `pip` to install the software on your target environment:

```commandline
pip install git+https://gitlab.com/acoupy/acoupy_meshutil.git
```

## Documentation

The full reference is available [here](https://acoupy.gitlab.io/acoupy_meshutil).
